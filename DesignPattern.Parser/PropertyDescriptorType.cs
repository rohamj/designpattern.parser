﻿namespace DesignPattern.Parser
{
    public enum PropertyDescriptorType
    {
        None = 0,
        Include = 1,
        Exclude = 2
    }
}
