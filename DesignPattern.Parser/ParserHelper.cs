﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.Serialization;

namespace DesignPattern.Parser
{
    /// <summary>
    /// ParserHelper Class
    /// </summary>
    public static class ParserHelper
    {
        /// <summary>
        /// Tries the parse.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value">The value.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        public static T TryParse<T>(this object value, object defaultValue = null)
        {
            T result;
            try
            {
                result = typeof(T) == typeof(object[]) && value.GetType() != typeof(object[])
                    ? (T)Convert.ChangeType(new[] { value }, typeof(T))
                    : (T)Convert.ChangeType(value, typeof(T));
            }
            catch (NullReferenceException)
            {
                result = SetDefault<T>(defaultValue);
            }
            catch (FormatException)
            {
                result = SetDefault<T>(defaultValue);
            }
            catch (InvalidCastException)
            {
                try
                {
                    if (typeof(T).IsGenericType && typeof(T).GetGenericTypeDefinition() == typeof(Nullable<>))
                    {
                        var converter = TypeDescriptor.GetConverter(typeof(T));
                        result = (T)converter.ConvertFromString(value.ToString());
                    }
                    else
                        result = SetDefault<T>(defaultValue);
                }
                catch (Exception)
                {
                    result = SetDefault<T>(defaultValue);
                }
            }

            return result;
        }

        /// <summary>
        /// Sets the default.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        public static T SetDefault<T>(object defaultValue = null)
        {
            T result;

            try
            {
                result = (T)defaultValue;
            }
            catch (NullReferenceException)
            {
                result = default(T);
            }
            catch (InvalidCastException)
            {
                result = default(T);
            }

            return result;
        }

        /// <summary>
        /// Gets the properties.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="propertyDescriptorType">Type of the property descriptor.</param>
        /// <param name="propertyDescriptors">The property descriptors.</param>
        /// <returns></returns>
        public static List<PropertyInfo> GetProperties<T>(
            PropertyDescriptorType propertyDescriptorType = PropertyDescriptorType.Include,
            params Expression<Func<T, object>>[] propertyDescriptors)
        {
            var finalList = new List<PropertyInfo>();
            var inputProps = new List<PropertyInfo>();
            var properties =
                (typeof(T)).GetProperties(BindingFlags.FlattenHierarchy | BindingFlags.Public | BindingFlags.Instance);

            if (propertyDescriptors != null && propertyDescriptors.Any())
            {
                inputProps.AddRange(from propertyDescriptor in propertyDescriptors
                                    let memeber = propertyDescriptor.Body as MemberExpression
                                    let unary = propertyDescriptor.Body as UnaryExpression
                                    select memeber ?? (unary != null ? unary.Operand as MemberExpression : null)
                    into memeber
                                    where memeber != null
                                    select memeber.Member as PropertyInfo);

                switch (propertyDescriptorType)
                {
                    case PropertyDescriptorType.Include:
                    case PropertyDescriptorType.None:
                        finalList.AddRange(
                            inputProps.Select(
                                incProp =>
                                    properties.FirstOrDefault(
                                        a =>
                                            String.Equals(a.Name, incProp.Name,
                                                StringComparison.CurrentCultureIgnoreCase)))
                                .Where(prop => prop != null && !String.IsNullOrEmpty(prop.Name)));
                        break;

                    case PropertyDescriptorType.Exclude:
                        finalList.AddRange(from property in properties
                                           let mustBeExluded =
                                               inputProps.Any(
                                                   x =>
                                                       String.Equals(property.Name, x.Name, StringComparison.CurrentCultureIgnoreCase))
                                           where !mustBeExluded
                                           select property);
                        break;
                }
            }
            else
                finalList.AddRange(properties);

            return finalList;
        }

        /// <summary>
        /// Gets the name of the attribute Column name and / or DataMember attribute name.
        /// </summary>
        /// <param name="property">The property.</param>
        /// <returns></returns>
        public static string GetAttributeName(this PropertyInfo property)
        {
            var attributeName = property.Name;

            var columnAttributes = property.GetCustomAttributes(typeof(ColumnAttribute), true);
            var columnAttribute = columnAttributes.Select(x => x).OfType<ColumnAttribute>().First();
            if (columnAttribute == null)
            {
                var dataMemberAttributes = property.GetCustomAttributes(typeof(DataMemberAttribute), true);
                var dataMemberAttribute = dataMemberAttributes.Select(x => x).OfType<DataMemberAttribute>().First();
                if (dataMemberAttribute != null)
                    attributeName = dataMemberAttribute.Name;
            }
            else
                attributeName = columnAttribute.Name;

            return attributeName;
        }
    }
}
